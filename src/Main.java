import javax.swing.*;

public class Main {

    static void setTheme(){
        try {
            UIManager.setLookAndFeel("com.bulenkov.darcula.DarculaLaf");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
    }

    static void initView(){
        UI _ui = new UI();
        _ui.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        _ui.setVisible(true);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                setTheme();
                initView();
            }
        });
    }
}