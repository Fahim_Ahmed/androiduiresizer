import javax.swing.plaf.*;
import com.mortennobel.imagescaling.AdvancedResizeOp;
import com.mortennobel.imagescaling.*;
import com.mortennobel.imagescaling.ResampleOp;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Vector;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.GroupLayout;
import javax.swing.filechooser.FileSystemView;
/*
 * Created by JFormDesigner on Fri May 15 21:39:16 BDT 2015
 */



/**
 * @author fahim ahmed
 */
public class UI extends JFrame {

    private JFileChooser fc;
    private Vector<File> droppedFiles;
    private Vector<File> pathFiles;
    private Vector<Thread> threadPool;

    private boolean isFileDropped;
    private boolean isProcessing;

    private int threadCounter;

    public UI() {
        initComponents();
        setIcon();

        isFileDropped = false;
        isProcessing = false;

        droppedFiles = new Vector<File>();
        pathFiles = new Vector<File>();
        threadPool = new Vector<Thread>();

        initDropZone();
    }

    private File showDirChooser(){
        if(fc == null) {
            fc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
            fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        }

        if(fc.showOpenDialog(UI.this) == JFileChooser.APPROVE_OPTION){
            return fc.getSelectedFile();
        }

        return null;
    }

    private void btnMainActionPerformed(ActionEvent e) {
        if(isProcessing) return;
        showStatus("Staring processes...");

        isProcessing = true;
        threadCounter = 0;
        threadPool.clear();

        if(tfSource.getText().trim().length() == 0) clearPathFiles();

        if(tfDest.getText().trim().length() == 0){
            //showWarning("Life is pointless like your destination.");
            showWarning("You are a little soul carrying around a corpse. - Epictetus\nBy the way, your life's destination is empty.");
            isProcessing = false;
            return;
        }

        boolean success = addFilesToList(pathFiles, tfSource.getText()); // scan and add all files to pathFiles
        if(!success && !isFileDropped){
            showWarning("I'm not your Enter button. Behave your rugged monstrosity!");
            isProcessing = false;
            return;
        }

        if(!createDirectoryStructure()){
            isProcessing = false;
            return;
        }

        processFiles();
       // isProcessing = false;
    }

    private void processFiles(){
        Vector<File> tmpList = new Vector<File>();
        tmpList.addAll(droppedFiles);
        tmpList.addAll(pathFiles);

        if(!tmpList.isEmpty()) {
            for (final File file : tmpList) {
                Thread t = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        synchronized (this) {
                            try {
                                sampleImages(file, 4 - comboBox1.getSelectedIndex());   // process each image in list
                            } catch (IOException ex) {
                                ex.printStackTrace();
                                showWarning("Error: Permission or invalid path. Sigh.");
                                isProcessing = false;

                                for (Thread t : threadPool) t.interrupt();

                            } catch (NullPointerException ex) {
                                ex.printStackTrace();
                                showWarning("Error: Permission or invalid path. Sigh.");
                                isProcessing = false;

                                for (Thread t : threadPool) t.interrupt();

                            } catch (IllegalStateException ex) {
                                threadCounter++;
                                System.out.println("-----> " + threadCounter);
                                if (threadPool.size() == threadCounter) {
                                    isProcessing = false;
                                    showStatus("Most probably it's all done.");
                                }
                            }

                        }
                    }
                });
                threadPool.add(t);
            }
        }else{
            showWarning("No file(s) to process.");
            isProcessing = false;
            return;
        }
        for (Thread t : threadPool) t.start();
    }

    private void sampleImages(final File file, final int rank) throws IOException, NullPointerException, IllegalStateException {
        showStatus("Reading file: " + file.getName());
        BufferedImage bigImage = ImageIO.read(file);

        float width = bigImage.getWidth();
        float height = bigImage.getHeight();
        float ratio = width / height;
        float mdpiSize = ratio >= 1.0 ? width / rank : height / rank;   //mdpi size
        if(rank == 1) mdpiSize = ratio >= 1.0 ? width / 1.5f : height / 1.5f;   //for weird 1.5x

        scaleDownImage(bigImage, rank, ratio, mdpiSize, file.getName());  // resize create multiple images from single image
    }

    private void scaleDownImage(BufferedImage bigImage, int rank, float ratio, float mdpiSize, String name) throws IOException, NullPointerException, IllegalStateException {
        name = name.substring(0, name.lastIndexOf("."));
        BufferedImage tmp;
        String dirName = "";

        for (int i = 0; i < rank; i++) {
            float multiplier = rank - i;

            if(i == 0) multiplier = 1.5f;

            float nextSize = mdpiSize * multiplier;

            float w, h;
            if (ratio >= 1.0) {
                w = nextSize;
                h = nextSize / ratio;
            } else {
                w = nextSize * ratio;
                h = nextSize;
            }

            //showStatus("Status: " + name + " - x" + multiplier + " - " + w + "x" + h);

            //MultiStepRescaleOp resampleOp = new MultiStepRescaleOp((int) w, (int)h);
            ResampleOp resampleOp = new ResampleOp((int) w, (int) h);
            resampleOp.setFilter(new BiCubicFilter());
            resampleOp.setUnsharpenMask(AdvancedResizeOp.UnsharpenMask.SemiNormal);
            tmp = resampleOp.filter(bigImage, null);

            if(multiplier == 3) dirName = "/drawable-xxhdpi";
            if(multiplier == 2) dirName = "/drawable-xhdpi";
            if(multiplier == 1) dirName = "/drawable-mdpi";
            if(multiplier == 1.5) dirName = "/drawable-hdpi";

            ImageIO.write(tmp, "png", new File(tfDest.getText() + dirName + "/" + name + ".png"));
        }

        if(rank == 4) dirName = "/drawable-xxxhdpi";
        if(rank == 3) dirName = "/drawable-xxhdpi";
        if(rank == 2) dirName = "/drawable-xhdpi";
        if(rank == 0) dirName = "/drawable-mdpi";

        ImageIO.write(bigImage, "png", new File(tfDest.getText() + dirName + "/" + name + ".png"));

        //Ajaira pain -_-
        if(rank == 1) {
            dirName = "/drawable-mdpi";

            float w, h;

            if (ratio >= 1.0) {
                w = mdpiSize;
                h = mdpiSize / ratio;
            } else {
                w = mdpiSize * ratio;
                h = mdpiSize;
            }

            ResampleOp resampleOp = new ResampleOp((int) w, (int) h);
            resampleOp.setFilter(new BiCubicFilter());
            resampleOp.setUnsharpenMask(AdvancedResizeOp.UnsharpenMask.SemiNormal);
            tmp = resampleOp.filter(bigImage, null);

            ImageIO.write(tmp, "png", new File(tfDest.getText() + dirName + "/" + name + ".png"));
        }

        showStatus("Status: " + name + " is most probably resampled.");
        throw new IllegalStateException();
    }

    private void btnResetActionPerformed(ActionEvent e) {
        if (isProcessing) return;

        pathFiles.clear();
        droppedFiles.clear();
        isFileDropped = false;
        dropzoneOverlay.setIcon(new ImageIcon(getClass().getResource("/images/dd_idle.png")));

        tfSource.setText("");

        showStatus("...zZ");
    }

    private boolean createDirectoryStructure() {
        File fDest = new File(tfDest.getText());
        if(!fDest.exists()){
            if (!fDest.mkdirs()) {
                showWarning("Error: Permission or invalid path.");
                return false;
            }
        }

        File fxxx = new File(tfDest.getText() + "/drawable-xxxhdpi");
        File fxx = new File(tfDest.getText() + "/drawable-xxhdpi");
        File fx = new File(tfDest.getText() + "/drawable-xhdpi");
        File fh = new File(tfDest.getText() + "/drawable-hdpi");
        File fm = new File(tfDest.getText() + "/drawable-mdpi");

        if(!fxxx.exists()) {
            if (!fxxx.mkdir()) {
                showWarning("Error: Permission or invalid path.");
                return false;
            }
        }

        if(!fxx.exists()) fxx.mkdir();
        if(!fx.exists()) fx.mkdir();
        if(!fh.exists()) fh.mkdir();
        if(!fm.exists()) fm.mkdir();

        return true;
    }

    private void clearPathFiles(){
        pathFiles.clear();
    }

    private void clearDroppedFiles(){
        droppedFiles.clear();
        isFileDropped = false;
        dropzoneOverlay.setIcon(new ImageIcon(getClass().getResource("/images/dd_idle.png")));
    }

    private boolean addFilesToList(Vector<File> files, String path){
        File file = new File(path);
        return addFilesToList(files, file);
    }

    private boolean addFilesToList(Vector<File> files, File file){
        //if(tfSource.getText().trim().length() == 0) return false;

        //File file = new File(path);

        if(file.exists()) {
            Collections.addAll(files, file.listFiles(new FileFilter() {
                @Override
                public boolean accept(File pathname) {
                    return pathname.getName().toLowerCase().endsWith(".jpg") || pathname.getName().toLowerCase().endsWith(".jpeg") || pathname.getName().toLowerCase().endsWith(".png");
                }
            }));
        }else {
            return false;
        }

        return true;
    }

    private void initDropZone() {
        new FileDrop(dropZone, BorderFactory.createEmptyBorder(), new FileDrop.Listener() {
            @Override
            public void filesDropped(File[] files) {
                clearDroppedFiles();

                for (File file : files) {
                    if(file.isDirectory()) addFilesToList(droppedFiles, file);
                    else if (file.getName().toLowerCase().endsWith(".jpg") || file.getName().toLowerCase().endsWith(".jpeg") || file.getName().toLowerCase().endsWith(".png")) {
                        droppedFiles.add(file);
                    }
                }

                isFileDropped = !droppedFiles.isEmpty();

                if(isFileDropped){
                    showStatus("File(s) dropped: " + droppedFiles.size());
                    dropzoneOverlay.setIcon(new ImageIcon(getClass().getResource("/images/dd_ready.png")));
                }else{
                    showWarning("Wrong file type.");
                    dropzoneOverlay.setIcon(new ImageIcon(getClass().getResource("/images/dd_idle.png")));
                    return;
                }

                if(fastMode.isSelected()) btnMainActionPerformed(null);
            }

            @Override
            public void onDragEnter() {
                dropzoneOverlay.setIcon(new ImageIcon(getClass().getResource("/images/dd_ondrag.png")));
            }

            @Override
            public void onDragExit() {
                if(isFileDropped){
                    dropzoneOverlay.setIcon(new ImageIcon(getClass().getResource("/images/dd_ready.png")));
                    return;
                }

                dropzoneOverlay.setIcon(new ImageIcon(getClass().getResource("/images/dd_idle.png")));
                isFileDropped = false;
            }
        });
    }

    void showWarning(String s){
        lStat.setForeground(new Color(255, 75, 111));
        lStat.setText(s);
        lStat.repaint();
    }

    void showStatus(String s){
        lStat.setForeground(new Color(0, 255, 153));
        lStat.setText(s);
        lStat.repaint();
    }

    private void btnSourceActionPerformed(ActionEvent e) {
        if(isProcessing) return;

        File file = showDirChooser();
        tfSource.setText(file.getPath());
        System.out.println("-----> " + file.getPath());

//        pathFiles.clear();
//        boolean success = addFilesToList(pathFiles, file);
//        if(!success) showWarning("Invalid directory or path.");
    }

    private void btnDestActionPerformed(ActionEvent e) {
        if(isProcessing) return;

        File file = showDirChooser();
        if(file != null) {
            tfDest.setText(file.getPath());
            //System.out.println("-----> " + path);
        }
    }

    private void btnClearActionPerformed(ActionEvent e) {
        tfSource.setText("");
    }

    private void setIcon(){
        try {
            ArrayList<Image> icons = new ArrayList<Image>();
            icons.add(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/icons/icon-128.png")));
            icons.add(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/icons/icon-96.png")));
            icons.add(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/icons/icon-72.png")));
            icons.add(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/icons/icon-64.png")));
            icons.add(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/icons/icon-48.png")));
            icons.add(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/icons/icon-32.png")));
            icons.add(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/icons/icon-24.png")));
            icons.add(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/icons/icon-16.png")));

            setIconImages(icons);
        } catch (IllegalArgumentException e){
            e.printStackTrace();
        }

        //super.setIconImage(image);
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        panel2 = new JPanel();
        panel1 = new JPanel();
        fastMode = new JCheckBox();
        label2 = new JLabel();
        tfSource = new JTextField();
        btnClear = new JButton();
        label4 = new JLabel();
        comboBox1 = new JComboBox();
        dropZone = new JPanel();
        dropzoneOverlay = new JLabel();
        label3 = new JLabel();
        tfDest = new JTextField();
        btnDest = new JButton();
        btnMain = new JButton();
        btnReset = new JButton();
        btnSource = new JButton();
        lStat = new JTextArea();

        //======== this ========
        setResizable(false);
        setTitle("A Shrink");
        setBackground(new Color(37, 39, 46));
        Container contentPane = getContentPane();

        //======== panel2 ========
        {
            panel2.setBackground(new Color(42, 44, 45));

            //======== panel1 ========
            {
                panel1.setBackground(new Color(42, 44, 45));

                //---- fastMode ----
                fastMode.setText("Fast Mode");
                fastMode.setToolTipText("...zZ");
                fastMode.setBackground(new Color(42, 44, 45));
                fastMode.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 11));

                //---- label2 ----
                label2.setText("Source Path");
                label2.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 11));
                label2.setToolTipText("src directory path");
                label2.setBackground(new Color(42, 44, 45));

                //---- tfSource ----
                tfSource.setToolTipText("Source directory path");
                tfSource.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 11));

                //---- btnClear ----
                btnClear.setText("Clear");
                btnClear.setToolTipText("...oO");
                btnClear.setBackground(new Color(42, 44, 45));
                btnClear.setForeground(new Color(255, 153, 51));
                btnClear.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 11));
                btnClear.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        btnClearActionPerformed(e);
                    }
                });

                //---- label4 ----
                label4.setText("Base Size");
                label4.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 11));

                //---- comboBox1 ----
                comboBox1.setModel(new DefaultComboBoxModel(new String[] {
                    "xxxhdpi",
                    "xxhdpi",
                    "xhdpi",
                    "hdpi",
                    "mdpi"
                }));
                comboBox1.setToolTipText("This not end here.");
                comboBox1.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 11));

                //======== dropZone ========
                {
                    dropZone.setBackground(new Color(42, 44, 45));
                    dropZone.setBorder(null);

                    //---- dropzoneOverlay ----
                    dropzoneOverlay.setIcon(new ImageIcon(getClass().getResource("/images/dd_idle.png")));

                    GroupLayout dropZoneLayout = new GroupLayout(dropZone);
                    dropZone.setLayout(dropZoneLayout);
                    dropZoneLayout.setHorizontalGroup(
                        dropZoneLayout.createParallelGroup()
                            .addGroup(GroupLayout.Alignment.TRAILING, dropZoneLayout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(dropzoneOverlay))
                    );
                    dropZoneLayout.setVerticalGroup(
                        dropZoneLayout.createParallelGroup()
                            .addComponent(dropzoneOverlay)
                    );
                }

                //---- label3 ----
                label3.setText("Destination");
                label3.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 11));
                label3.setToolTipText("src directory path");
                label3.setBackground(new Color(42, 44, 45));

                //---- tfDest ----
                tfDest.setToolTipText("Source directory path");
                tfDest.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 11));

                //---- btnDest ----
                btnDest.setText("O ");
                btnDest.setToolTipText("...oO");
                btnDest.setBackground(new Color(42, 44, 45));
                btnDest.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        btnDestActionPerformed(e);
                    }
                });

                //---- btnMain ----
                btnMain.setText("JUST DO IT");
                btnMain.setBackground(new Color(42, 45, 45));
                btnMain.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 11));
                btnMain.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        btnMainActionPerformed(e);
                    }
                });

                //---- btnReset ----
                btnReset.setText("RESET");
                btnReset.setBackground(new Color(42, 45, 45));
                btnReset.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 11));
                btnReset.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        btnResetActionPerformed(e);
                    }
                });

                //---- btnSource ----
                btnSource.setText("O ");
                btnSource.setToolTipText("...oO");
                btnSource.setBackground(new Color(42, 44, 45));
                btnSource.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        btnSourceActionPerformed(e);
                    }
                });

                GroupLayout panel1Layout = new GroupLayout(panel1);
                panel1.setLayout(panel1Layout);
                panel1Layout.setHorizontalGroup(
                    panel1Layout.createParallelGroup()
                        .addGroup(panel1Layout.createSequentialGroup()
                            .addContainerGap()
                            .addGroup(panel1Layout.createParallelGroup()
                                    .addComponent(label2, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                                    .addComponent(label4, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(panel1Layout.createParallelGroup()
                                    .addGroup(panel1Layout.createSequentialGroup()
                                            .addComponent(tfSource, GroupLayout.PREFERRED_SIZE, 217, GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(btnSource, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(btnClear))
                                    .addGroup(panel1Layout.createSequentialGroup()
                                            .addComponent(comboBox1)
                                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(fastMode, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)))
                            .addContainerGap())
                        .addGroup(panel1Layout.createSequentialGroup()
                            .addGroup(panel1Layout.createParallelGroup()
                                .addComponent(dropZone, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addGroup(panel1Layout.createSequentialGroup()
                                    .addGap(5, 5, 5)
                                    .addComponent(label3, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                                    .addGap(5, 5, 5)
                                    .addComponent(tfDest, GroupLayout.PREFERRED_SIZE, 280, GroupLayout.PREFERRED_SIZE)
                                    .addGap(5, 5, 5)
                                    .addComponent(btnDest))
                                .addComponent(btnMain, GroupLayout.PREFERRED_SIZE, 420, GroupLayout.PREFERRED_SIZE)
                                .addComponent(btnReset, GroupLayout.PREFERRED_SIZE, 420, GroupLayout.PREFERRED_SIZE))
                            .addGap(0, 0, Short.MAX_VALUE))
                );
                panel1Layout.setVerticalGroup(
                    panel1Layout.createParallelGroup()
                        .addGroup(panel1Layout.createSequentialGroup()
                            .addGap(7, 7, 7)
                            .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                                .addComponent(label4)
                                .addComponent(fastMode)
                                .addComponent(comboBox1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(btnClear)
                                .addComponent(label2)
                                .addComponent(btnSource)
                                .addComponent(tfSource, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(dropZone, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(panel1Layout.createParallelGroup()
                                .addGroup(panel1Layout.createSequentialGroup()
                                    .addGap(10, 10, 10)
                                    .addComponent(label3))
                                .addGroup(panel1Layout.createSequentialGroup()
                                    .addGap(5, 5, 5)
                                    .addComponent(tfDest, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                .addComponent(btnDest))
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(btnMain, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(btnReset, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                            .addContainerGap())
                );
            }

            //---- lStat ----
            lStat.setText("...zZ");
            lStat.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
            lStat.setForeground(new Color(0, 255, 153));
            lStat.setFocusable(false);
            lStat.setRequestFocusEnabled(false);
            lStat.setLineWrap(true);
            lStat.setEditable(false);
            lStat.setCaretPosition(4);
            lStat.setWrapStyleWord(true);
            lStat.setBackground(new Color(42, 45, 45));

            GroupLayout panel2Layout = new GroupLayout(panel2);
            panel2.setLayout(panel2Layout);
            panel2Layout.setHorizontalGroup(
                panel2Layout.createParallelGroup()
                    .addGroup(panel2Layout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addGroup(panel2Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                            .addGroup(panel2Layout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addComponent(lStat))
                            .addComponent(panel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(32, Short.MAX_VALUE))
            );
            panel2Layout.setVerticalGroup(
                panel2Layout.createParallelGroup()
                    .addGroup(GroupLayout.Alignment.TRAILING, panel2Layout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addComponent(panel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lStat, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                        .addGap(13, 13, 13))
            );
        }

        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
            contentPaneLayout.createParallelGroup()
                .addComponent(panel2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        contentPaneLayout.setVerticalGroup(
            contentPaneLayout.createParallelGroup()
                .addComponent(panel2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        setSize(500, 560);
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JPanel panel2;
    private JPanel panel1;
    private JCheckBox fastMode;
    private JLabel label2;
    private JTextField tfSource;
    private JButton btnClear;
    private JLabel label4;
    private JComboBox comboBox1;
    private JPanel dropZone;
    private JLabel dropzoneOverlay;
    private JLabel label3;
    private JTextField tfDest;
    private JButton btnDest;
    private JButton btnMain;
    private JButton btnReset;
    private JButton btnSource;
    private JTextArea lStat;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
